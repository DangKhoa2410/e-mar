const navLink = document.getElementById('nav-link');
const navLinkIntro = document.getElementById('nav-link-introduce');
const navLink3 = document.getElementById('nav-link3');
const navLink4 = document.getElementById('nav-link4');
navLink.addEventListener('click', (e) => {
  e.preventDefault()
  window.location.href = '../../index.html'
})

navLinkIntro.addEventListener('click', (e) => {
  e.preventDefault()
  window.location.href = '../../page/introduce.html'
})

navLink3.addEventListener('click', (e) => {
  e.preventDefault();
  window.location.href = '../../page/guarantee.html'
})

navLink4.addEventListener('click', (e) => {
  e.preventDefault();
  window.location.href = '../../page/consultation.html'
})


